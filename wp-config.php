<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'project-wordpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'z26F)# vVK -ZIc5c)SFy1!#YNPv8KHi-@B`JpJg+nJcr*3&]g#qqW%fhYoTkPk|');
define('SECURE_AUTH_KEY',  '|(O2<l49b-+Y{woRv2+$v=@*Zk~r2kZf|T0=<F@y_][D%|FKfy/-CSeaJk/[a+*M');
define('LOGGED_IN_KEY',    '.-KZm1B*tW=UBj3=rCd}[Jm59+Bh&`Vm.XyA+v28|%[sp]k[4*qfrDO(yjrD+$?j');
define('NONCE_KEY',        '<uenyx$-`+<l=&_df/2&+hPy[+)B`<D)t, S6EF&d>zM:o.IIs&l]8q=iHNVcPl^');
define('AUTH_SALT',        '3gmUB/`A QgCzMn%7j ay!_q-Kw_L(sY{{yV)&=aeK_rdXcFgs`;l4Fmyx;,5NFh');
define('SECURE_AUTH_SALT', '0nY%LG:&imgOC#o-0$pp:TYhh6$|SP7Sci=lb:gV|L_wMq$& G+PT+3k-+>F;4:~');
define('LOGGED_IN_SALT',   '}}?>-#z.N)|G8h{yScd09DG}[QW0.P{-lIN|:$;v`PXOmz4~1dP|jB1cuO25*?9+');
define('NONCE_SALT',       '>x]h8e:sVi}1GxlYNtS+q#!A-@iV*Fj{#+-_N_l+sN8wodkYLU-m-P=vi`xS^,W)');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
