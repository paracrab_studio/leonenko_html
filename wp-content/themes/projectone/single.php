<?php get_header(); ?>
    <main>
        <div class="container">
            <div class="row">
                <!--CENTER-LEFT-MAIN-->
                <article class="col-xs-12 col-sm-8 col-md-9 col-lg-9">
                    <div class="row">
                        <div class="left-cont col-xs-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                <span class="text-uppercase"><h2><?php the_title(); ?></h2></span>
                                </div>
                                <div class="panel-body">
                                    <?php while (have_posts()): the_post();?>
                                        <?php
                                        $img_post = get_field('img_post');
                                        $quadro_img = get_field('quadro_img');
                                        if( !empty($img_post) ): ?>
                                            <img class="img-rounded img-responsive col-xs-12" src="<?php the_field('img_post'); ?>" alt="<?php echo $img_post['alt']; ?>"/>
                                            <img class="img-rounded img-responsive col-xs-12 col-md-6" src="<?php the_field('img_post'); ?>" alt="<?php echo $img_post['alt']; ?>"/>
                                            <img class="img-rounded img-responsive col-xs-12 col-md-6" src="<?php the_field('img_post'); ?>" alt="<?php echo $img_post['alt']; ?>"/>
                                        <?php elseif( !empty($quadro_img) ): ?>
                                            <div class="img-quadro container-fluid">
                                                <div class="row">
                                                    <img class="img-rounded img-responsive col-xs-6" src="<?php the_field('quadro_img'); ?>" alt="<?php echo $img_post['alt']; ?>"/>
                                                    <img class="img-rounded img-responsive col-xs-6" src="<?php the_field('quadro_img_2'); ?>" alt="<?php echo $img_post['alt']; ?>"/>
                                                    <img class="img-rounded img-responsive col-xs-6" src="<?php the_field('quadro_img_3'); ?>" alt="<?php echo $img_post['alt']; ?>"/>
                                                    <img class="img-rounded img-responsive col-xs-6" src="<?php the_field('quadro_img_4'); ?>" alt="<?php echo $img_post['alt']; ?>"/>
                                                </div>
                                            </div>
                                        <?php endif; ?>
                                        <div class="clearfix"></div>
                                        <p><?php the_content();?></p>
                                        <p><?php the_time('F j, Y'); ?> at <?php the_time('g:i a'); ?> | <?php the_category(', '); ?> | <?php comments_number('No comment', '1 comment', '% comments'); ?></p>
                                    <?php endwhile; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
                <?php get_sidebar() ?>
            </div>
        </div>
    </main>
<?php get_footer(); ?>