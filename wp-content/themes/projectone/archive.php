<?php get_header(); ?>
    <main>
        <div class="container">
            <div class="row">
                <!--CENTER-LEFT-MAIN-->
                <article class="col-xs-12 col-sm-8 col-md-9 col-lg-9">
                    <div class="row">
                        <div class="left-cont col-xs-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                <span class="text-uppercase">
                                    <?php the_title(); ?>
                                </span>
                                </div>
                                <div class="panel-body">
                                    <h1 class="page-title">
                                        <?php
                                        if (is_category()):
                                            single_cat_title();
                                        elseif ( is_tag() ) :
                                            single_tag_title();
                                        elseif (is_year()):
                                            printf(__('Year: %s', 'striped'), '<span>' . get_the_date(_x('Y', 'yearly archives date format', 'striped')) . '</span>');
                                        elseif (is_month()):
                                            printf(__('Month: %s', 'striped'), '<span>' . get_the_date(_x('F Y', 'monthly archives date format', 'striped')) . '</span>');
                                        elseif (is_day()):
                                            printf(__('Day: %s', 'striped'), '<span>' . get_the_date() . '</span>');
                                        elseif (is_author()):
                                            printf( __( 'Author: %s', 'striped' ), '<span class="vcard"><a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '" title="' . esc_attr( get_the_author() ) . '" rel="me">' . get_the_author() . '</a></span>' );
                                        else :
                                            _e( 'Archives', 'striped' );
                                        endif;
                                        ?>
                                    </h1>
                                    <?php while ( have_posts() ) : the_post(); ?>
                                        <?php the_content(); ?>
                                    <?php endwhile; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
                <?php get_sidebar() ?>
            </div>
        </div>
    </main>
<?php get_footer(); ?>