<?php

function enqueue_styles() {
    wp_register_style( 'bootstrap-css', get_template_directory_uri() . '/bootstrap/css/bootstrap.min.css');
    wp_enqueue_style('bootstrap-css');
    wp_enqueue_style('bootstrap-responsive', get_template_directory_uri() . '/css/bootstrap-responsive.css', array('bootstrap-css'));
    wp_enqueue_style('font-awesome', '//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css');
}
add_action('wp_enqueue_scripts', 'enqueue_styles');

function theme_register_nav_menu() {
    register_nav_menu( '', '' );
}
add_action( 'after_setup_theme', 'theme_register_nav_menu' );

remove_filter('the_content', 'wpautop');

if ( function_exists( 'add_theme_support' ) ) {
    add_theme_support( 'post-thumbnails' );
}

function register_my_widgets(){
    register_sidebar( array(
        'name' => "Правая боковая панель сайта",
        'id' => 'right-sidebar',
        'description' => 'Эти виджеты будут показаны с правой колонке сайта',
        'before_widget' => '<div class="panel panel-default"><div class="panel-body">',
        'after_widget' => '</div></div>',
        'before_title' => '<span class="text-uppercase">',
        'after_title' => '</span>',
    ) );
}
add_action( 'widgets_init', 'register_my_widgets' );

function header_slider()
{
    $labels = array(
        'name' => 'Header slider',
        'singular_name' => 'Header slider',
        'add_new' => 'Add header slider',
        'add_new_item' => 'Add new header slider',
        'edit_item' => 'edit header slider',
        'new_item' => 'New header slider',
        'view_item' => 'view header slider',
        'search_items' => 'search header slider',
        'not_found' =>  'Header slider not found',
        'not_found_in_trash' => 'В корзине header slider не найдено',
        'parent_item_colon' => '',
        'menu_name' => 'Header slider'
    );

    $args = array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'rewrite' => true,
        'capability_type' => 'post',
        'has_archive' => true,
        'hierarchical' => false,
        'menu_position' => 5,
        'supports' => array('title','editor','thumbnail', 'comments'),
        'taxonomies' => array('header_slider')
    );
    register_post_type('header_slider',$args);
}
add_action('init', 'header_slider');

function enqueue_scripts() {
    global $wp_scripts;
    wp_register_script('html5-shim', 'http:html5shim.googlecode.com/svn/trunk/html5.js');
    wp_register_script('html5-respond', 'https://oss.maxcdn.com/respond/1.4.2/respond.min.js');
    wp_register_script('images-loaded', get_template_directory_uri() . '/js/imagesloaded.pkgd.min.js', 'jquery');
    wp_register_script('masonry-js', get_template_directory_uri() . '/js/masonry.pkgd.min.js', array('images-loaded'));
    wp_enqueue_script('html5-shim');
    wp_enqueue_script('html5-respond');
    $wp_scripts->add_data( 'html5-shim', 'conditional', 'lt IE 9' );
    $wp_scripts->add_data( 'html5-respond', 'conditional', 'lt IE 9' );
    wp_enqueue_script('jquery');
    wp_enqueue_script('myscript', get_template_directory_uri() . '/js/myscript.js', array('jquery') );
    wp_enqueue_script('images-loaded');
    wp_enqueue_script('bootstrap', get_template_directory_uri() . '/bootstrap/js/bootstrap.min.js', array('jquery'));
    wp_enqueue_script('masonry-js');
}
add_action('wp_enqueue_scripts', 'enqueue_scripts');






