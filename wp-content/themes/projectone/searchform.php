<form name="search" action="<?php echo home_url( '/' ); ?>" method="get" role="search"  class="search-form" >
    <div class="input-group">
        <input type="text" class="form-control" name="s" />
        <span class="input-group-btn">
            <button class="btn btn-default" type="submit"><span class="fa fa-search fa-lg"></span></button>
        </span>
    </div>
</form>