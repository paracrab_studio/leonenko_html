<!DOCTYPE html>
<html>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php bloginfo('name'); ?><?php wp_title( '|', true, 'right' ); ?></title>
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<header>
    <div class="container-fluid">
        <nav role="navigation" class="navbar navbar-inverse">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="container">
                <div id="logo">
                    <a href="<?php echo site_url(); ?>"><?php bloginfo('name'); ?></a>
                </div>
                <div id="header-row" class="row">
                    <div class="navbar-header">
                        <button type="button" data-target="#navbar-collapse-btn" data-toggle="collapse" class="navbar-toggle">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <!-- Collection of nav links, forms, and other content for toggling -->
                    <div id="navbar-collapse-btn" class="collapse navbar-collapse">
                        <?php
                            wp_nav_menu( array(
                                    'container'       => 'false',
                                    'menu_class'      => 'nav navbar-nav navbar-right'
                            ));
                        ?>
                    </div>
                </div>
            </div>
        </nav>
    </div>
    <?php if (is_home() || is_front_page()): ?>
        <div class="container">
            <div class="row">
                <div id="carousel-example-generic" class="carousel slide hidden-xs" data-ride="carousel">
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner">
                        <?php
                        $header_slider = new WP_Query( array( 'post_type' => 'header_slider', 'posts_per_page' => 10 ) );
                        while ($header_slider->have_posts() ) : $header_slider-> the_post();?>
                            <div class="item">
                                <img class="thumbnail img-responsive" src="<?php the_field('img_slider'); ?>" alt="banner"/>
                                    <div class="carousel-caption">
                                        <span class="fa fa-user fa-5x hidden-xs hidden-sm hidden-md"></span>
                                        <h1 class="text-uppercase hidden-xs hidden-sm"><?php the_title();?></h1>
                                        <p class="hidden-xs hidden-sm"><?php the_content();?></p>
                                        <a href="<?php the_permalink(); ?>"><button class="btn text-uppercase hidden-xs hidden-sm">laoreet ipun</button></a>
                                    </div>
                            </div>
                        <?php endwhile;?>


<!--                        <div class="item">-->
<!--                            <img class="thumbnail img-responsive" src="--><?php //bloginfo('template_url') ?><!--/images/banner.jpg" alt="banner">-->
<!--                            <div class="carousel-caption">-->
<!--                                <span class="fa fa-user fa-5x hidden-xs hidden-sm hidden-md"></span>-->
<!--                                <h1 class="text-uppercase hidden-xs hidden-sm">Nunc digno lorem dol</h1>-->
<!--                                <p class="hidden-xs hidden-sm">Vivamus vestibulum nulla</p>-->
<!--                                <button class="btn text-uppercase hidden-xs hidden-sm">laoreet ipun</button>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                        <div class="item">-->
<!--                            <img class="thumbnail img-responsive" src="--><?php //bloginfo('template_url') ?><!--/images/banner.jpg" alt="banner">-->
<!--                            <div class="carousel-caption">-->
<!--                                <span class="fa fa-user fa-5x hidden-xs hidden-sm hidden-md"></span>-->
<!--                                <h1 class="text-uppercase hidden-xs hidden-sm">Nunc digno lorem dol</h1>-->
<!--                                <p class="hidden-xs hidden-sm">Vivamus vestibulum nulla</p>-->
<!--                                <button class="btn text-uppercase hidden-xs hidden-sm">laoreet ipun</button>-->
<!--                            </div>-->
<!--                        </div>-->
                    </div>
                    <!-- Controls -->
                    <div class="controls">
                        <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left"></span>
                        </a>
                        <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right"></span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>
</header>