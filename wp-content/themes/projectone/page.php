<?php get_header(); ?>
<main>
    <div class="container">
        <div class="row">
            <!--CENTER-LEFT-MAIN-->
            <article class="col-xs-12 col-sm-8 col-md-9 col-lg-9">
                <div class="row">
                    <div class="left-cont col-xs-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                            <span class="text-uppercase">
                                <h2><?php the_title(); ?></h2>
                            </span>
                            </div>
                            <div class="panel-body">
                                <?php if (have_posts()): while (have_posts()): the_post(); ?>
                                    <p><?php the_content(); ?></p>
                                <?php endwhile; endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </article>
            <?php get_sidebar() ?>
        </div>
    </div>
</main>
<?php get_footer(); ?>