<main>
    <div class="container">
        <div class="row">
            <!--TOP-MAIN-->
            <ul id="top-content" class="list-inline">
                <li class="col-xs-3 col-sm-3 col-md-3 col-lg-3 text-center">
                    <span class="fa fa-headphones fa-4x"></span>
                    <h4 class="text-uppercase">IBUS TORTOR</h4>
                    <a href="#">
                        <p><ins>Morbi interdum mollis</ins></p>
                    </a>
                </li>
                <li class="col-xs-3 col-sm-3 col-md-3 col-lg-3 text-center">
                    <span class="fa fa-refresh fa-4x"></span>
                    <h4 class="text-uppercase">ALICUAM</h4>
                    <a href="#">
                        <p><ins>Vestibulam dipbus</ins></p>
                    </a>
                </li>
                <li class="col-xs-3 col-sm-3 col-md-3 col-lg-3 text-center">
                    <span class="fa fa-clock-o fa-flip-horizontal fa-4x"></span>
                    <h4 class="text-uppercase">IBUS TORTOR</h4>
                    <a href="#">
                        <p><ins>Idot crasorma elinit</ins></p>
                    </a>
                </li>
                <li class="col-xs-3 col-sm-3 col-md-3 col-lg-3 text-center">
                    <span class="fa fa-comments-o fa-4x"></span>
                    <h4 class="text-uppercase">IBUS TORTOR</h4>
                    <a href="#">
                        <p><ins>Felis moodo tortor</ins></p>
                    </a>
                </li>
            </ul>
            <!--CENTER-LEFT-MAIN-->
            <article class="col-xs-12 col-sm-8 col-md-9 col-lg-9">

                <div class="js-masonry row">
                    <?php while ( have_posts() ) : the_post(); ?>
                        <div class="left-cont col-xs-6 col-sm-6 col-md-4 col-lg-4">
                            <div class="panel panel-default" <?php post_class('post'); ?> id="post-<?php the_ID(); ?>">
                                <div class="panel-body">
                                    <span class="text-uppercase">
                                        <?php the_title(); ?>
                                    </span>
                                    <?php
                                    $img_post = get_field('img_post');
                                    $quadro_img = get_field('quadro_img');
                                    if( !empty($img_post) ): ?>
                                        <img class="img-rounded img-responsive" src="<?php the_field('img_post'); ?>" alt="<?php echo $img_post['alt']; ?>"/>
                                    <?php elseif( !empty($quadro_img) ): ?>
                                        <div class="img-quadro container-fluid">
                                            <div class="row">
                                                <img class="img-rounded img-responsive col-xs-6" src="<?php the_field('quadro_img'); ?>" alt="<?php echo $img_post['alt']; ?>"/>
                                                <img class="img-rounded img-responsive col-xs-6" src="<?php the_field('quadro_img_2'); ?>" alt="<?php echo $img_post['alt']; ?>"/>
                                                <img class="img-rounded img-responsive col-xs-6" src="<?php the_field('quadro_img_3'); ?>" alt="<?php echo $img_post['alt']; ?>"/>
                                                <img class="img-rounded img-responsive col-xs-6" src="<?php the_field('quadro_img_4'); ?>" alt="<?php echo $img_post['alt']; ?>"/>
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                    <p><?php the_excerpt(); ?></p>
                                    <span>Posted on <time datetime="<?php the_date();?>"><?php the_date();?> at <?php the_time();?></time> by <?php the_author();?></span>
                                </div>
                                <div class="panel-footer"><a href="<?php the_permalink(); ?>">READ MORE <i class="fa fa-arrow-right"></i></a></div>
                            </div>
                        </div>
                    <?php endwhile; ?>
                </div>

            </article >
            <?php get_sidebar() ?>
        </div>
    </div>
</main>