<?php get_header(); ?>
<main>
    <div class="container">
        <div class="row">
            <!--CENTER-LEFT-MAIN-->
            <article class="col-xs-12 col-sm-8 col-md-9 col-lg-9">
                <div class="row">
                    <div class="left-cont col-xs-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <span class="text-uppercase">
                                    Error 404
                                </span>
                            </div>
                            <div class="panel-body">
                                <p><?php echo __('It looks like nothing was found at this location.'); ?></p>
                            </div>
                        </div>
                    </div>
                </div>
            </article>
            <?php get_sidebar() ?>
        </div>
    </div>
</main>
<?php get_footer(); ?>
