<!--CENTER-RIGHT-MAIN-->
<aside class="col-xs-12 col-sm-4 col-md-3 col-lg-3">
    <div class="row">
        <?php
        if ( function_exists('dynamic_sidebar') )
            dynamic_sidebar('right-sidebar');
        ?>
        <div class="panel panel-default">
            <div class="panel-body">
                <span class="text-uppercase">Maoreet facilis</span>
                <div class="panel-line">
                    <i class="fa fa-bell fa-2x fa-border pull-left"></i>
                    <h6 class="text-uppercase">Vivamus</h6>
                    <a href="#"><p><ins>Nulla nec praesent placerat risus..</ins></p></a>
                </div>
                <div class="panel-line">
                    <i class="fa fa-beer fa-2x fa-border pull-left"></i>
                    <h6 class="text-uppercase">John</h6>
                    <a href="#"><p><ins>Nulla nec praesent placerat risus..</ins></p></a>
                </div>
                <div class="panel-line">
                    <i class="fa fa-quote-right fa-2x fa-border pull-left"></i>
                    <h6 class="text-uppercase">Amenda</h6>
                    <a href="#"><p><ins>Nulla nec praesent placerat risus..</ins></p></a>
                </div>
            </div>
        </div>

    </div>
</aside>