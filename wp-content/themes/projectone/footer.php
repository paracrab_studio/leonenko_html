<footer>
    <div class="container">
        <div class="row">
            <ul class="list-inline">
                <li class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
                    <span><b>Copyright (c) <?php echo date('Y'); ?></b></span>
                    <p><a href="#">All rights reserved. </a></p>
                </li>
                <li class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
                    <span><b>Designed by:</b></span>
                    <p><a href="http://alltemplateneeds.com">www.alltemplateneeds.com </a></p>
                </li>
                <li class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
                    <span><b>Images From:</b></span>
                    <p><a href="http://wallcoo.net">wallcoo.net</a> | <a href="http://wallpaperswide.com">wallpaperswide.com</a></p>
                </li>
                <li class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
                    <span><b>Contact</b></span>
                    <p><a href="http://info@companyname.com">info@companyname.com</a></p>
                </li>
            </ul>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <ul class="list-inline">
                <li class="col-xs-6 col-sm-4 col-md-4 col-lg-3">
                    <a href="http://facebook.com/"><i class="fa fa-facebook-square fa-2x"> </i></a>
                    <a href="http://twitter.com/"><i class="fa fa-twitter-square fa-2x"> </i></a>
                    <a href="https://www.linkedin.com/"><i class="fa fa-linkedin-square fa-2x"> </i></a>
                    <a href="https://ru.pinterest.com/"><i class="fa fa-pinterest-square fa-2x"> </i></a>
                    <a href="http://all-phone.com.ua/index.php"><i class="fa fa-phone-square fa-2x"> </i></a>
                </li>
                <li class="col-xs-6 col-sm-4 col-md-4 col-lg-3">
                    <span><b><a href="/"><?php echo __('Home', 'Sitename'); ?></a> :: <a href="/about/"><?php echo __('About', 'Sitename'); ?></a> :: <a href="/contact/"><?php echo __('Contact', 'Sitename'); ?></a></b></span>
                </li>
            </ul>
        </div>
    </div>

</footer>
<?php wp_footer () ?>
</body>
</html>