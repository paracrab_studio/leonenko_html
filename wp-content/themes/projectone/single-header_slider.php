<?php get_header(); ?>
    <main>
        <div class="container">
            <div class="row">
                <!--CENTER-LEFT-MAIN-->
                <article class="col-xs-12 col-sm-8 col-md-9 col-lg-9">
                    <div class="row">
                        <div class="left-cont col-xs-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <span class="text-uppercase"><h2><?php the_title(); ?></h2></span>
                                </div>
                                <div class="panel-body">
                                    <?php while (have_posts()): the_post();?>
                                        <img class="img-responsive col-xs-12" src="<?php the_field('img_slider'); ?>" alt="<?php echo $img_slider['alt']; ?>"/>
                                        <div class="clearfix"></div>
                                        <p><?php the_content();?></p>
                                        <p><?php the_time('F j, Y'); ?> at <?php the_time('g:i a'); ?> | <?php the_category(', '); ?> | <?php comments_number('No comment', '1 comment', '% comments'); ?></p>
                                    <?php endwhile; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
                <?php get_sidebar() ?>
            </div>
        </div>
    </main>
<?php get_footer(); ?>