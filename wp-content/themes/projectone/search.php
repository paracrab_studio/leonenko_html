<?php get_header(); ?>
<main>
    <div class="container">
        <div class="row">
            <!--CENTER-LEFT-MAIN-->
            <article class="col-xs-12 col-sm-8 col-md-9 col-lg-9">
                <div class="row">
                    <div class="left-cont col-xs-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <span class="text-uppercase">
                                    <span><h2><?php printf( __('Search Results for: %s', 'default'), get_search_query() ); ?></h2></span>
                                </span>
                            </div>
                            <div class="panel-body">
                                <?php if (have_posts()): while (have_posts()): the_post(); ?>
                                    <span><h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3></span>
                                    <p><?php the_excerpt(); ?></p>
                                <?php endwhile; else: ?>
                                    <span><?php echo __('Sorry, no results found', 'whitesquare'); ?></span>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </article>
            <?php get_sidebar() ?>
        </div>
    </div>
</main>
<?php get_footer(); ?>